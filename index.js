var path = require('path');
const fs = require('fs-extra');
canDownload = false;
var gcs = require('@google-cloud/storage')({
  projectId: 'se371-163804',
  keyFilename: 'se371-cc5dab809342.json'
});
imagePath = '';
var bucket = gcs.bucket('ocr_data_image_files');
var bucket1 = gcs.bucket('ocr_text_data');
document.ondragover = document.ondrop = (ev) => {
  ev.preventDefault();
  ev.stopPropagation();
}
//$(document).ready(function(){
document.body.ondrop = (ev) => {
  imagePath = (ev.dataTransfer.files[0].path);
  if (path.extname(imagePath.toString().toLowerCase()) == '.jpg' || path.extname(imagePath.toString().toLowerCase()) == '.jpeg') {
    $('#boxtitle').empty();
    $('#boxtitle').prepend('<p>File is uploading</p>');
    $('#csvdiv').empty();
    $('#csvdiv').prepend('<p>Results will be here</p>');
    bucket.upload(imagePath.toString(), function(err, file) {
      if (!err) {
        $('#boxtitle').empty();
        $('#boxtitle').prepend('<p>File is uploaded</p>');
                canDownload = true;
        checkImage(imagePath);

      } else {
        alert('Something happen in the uploading');
        canDownload = false;
        alert(err.toString());
      }
    });
    ev.preventDefault();
    ev.stopPropagation();
  }
  else {
    alert('file is not jpg. Please upload a jpg');
    canDownload = false;
  }
}
//});
function checkImage(fileName) {
  if(canDownload == true)
  {
  strippedImage = path.basename(fileName).toString();
  csvImage = strippedImage + '_to_en.csv';
  fileCheck = bucket1.file(csvImage);
  exists = fileCheck.exists(function(err, exists) {});
  while (exists == false) {
    checkImage(fileName);
  }
  $('#csvdiv').empty();
  $('#csvdiv').prepend('<p>Image is processed. Click the download button.</p>');
}
else{
  alert('You cannot download before you add an image. Please add a jpg')
}
}

function downloadImage() {
  if(canDownload == true)
  {
  strippedPath = path.basename(imagePath).toString();
  csvPath = strippedPath + '_to_en.csv';
  //    alert(csvPath);
  file = bucket1.file(csvPath);
  //    alert(file.toString);
  if (file.exists().then(asdfImage(csvPath))) {
    //  var exists = data[0];
    console.log("file exists");
  }
}
else{
  alert('You cannot download before you add an image. Please add a jpg')
}
}

function asdfImage(path) {
  //    alert(path);
  var filepath = "results.csv";// Previously saved path somewhere

if (fs.existsSync(filepath)) {
    fs.unlinkSync(filepath);
} else {
    console.log("This file doesn't exist, cannot delete");
}
  bucket1.file(path).download({
    destination: 'results.csv'
  }, function(err) {});
  $('#csvdiv').empty();
  $('#csvdiv').prepend('<a href="results.csv" download><img  src="file.png" /></a></br><p>Click the file icon</p>');
}
